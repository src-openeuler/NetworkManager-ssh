%bcond_with libnm_glib
%global __provides_exclude ^libnm-.*\\.so

Name:          NetworkManager-ssh
Summary:       NetworkManager VPN plugin for SSH
Version:       1.2.12
Release:       2
License:       GPLv2+
URL:           https://github.com/danfruehauf/NetworkManager-ssh
Source0:       https://github.com/danfruehauf/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

Patch1001:     0001-fix-compile-Fix-configure.ac-error-causing-compilati.patch

BuildRequires: make
BuildRequires: autoconf
BuildRequires: gtk3-devel
BuildRequires: NetworkManager-libnm-devel >= 1:1.2.6
BuildRequires: glib2-devel
BuildRequires: libtool intltool gettext
BuildRequires: libnma-devel >= 1.1.0
BuildRequires: libsecret-devel
BuildRequires: libtool intltool gettext
# Both polycube & iptables provide libxtables.so.12
# We want iptables here
BuildRequires: iptables-libs

Requires:      gtk3
Requires:      dbus
Requires:      NetworkManager >= 1:1.2.6
Requires:      openssh-clients
Requires:      shared-mime-info
Requires:      sshpass

%if %with libnm_glib
BuildRequires: NetworkManager-glib-devel >= 1:1.2.6
BuildRequires: libnm-gtk-devel >= 0.9.10
%endif

%description
This package contains software for integrating VPN capabilities with
the OpenSSH server with NetworkManager.

%package -n NetworkManager-ssh-gnome
Summary:       NetworkManager VPN plugin for SSH - GNOME files
Requires:      %{name}%{?_isa} = %{version}-%{release}

%description -n NetworkManager-ssh-gnome
This package contains software for integrating VPN capabilities with
the OpenSSH server with NetworkManager (GNOME files).

%prep
%autosetup -p1

%build
if [ ! -f configure ]; then
  autoreconf -fvi
fi

%configure \
        --disable-static \
%if %without libnm_glib
        --without-libnm-glib \
%endif
        --enable-more-warnings=yes \
        --with-dist-version=%{version}-%{release}
make %{?_smp_mflags}

%install
make DESTDIR=%{buildroot} INSTALL="install -p" CP="cp -p" install

rm -f %{buildroot}%{_libdir}/NetworkManager/lib*.la

%find_lang %{name}

%files -f %{name}.lang
%{_sysconfdir}/dbus-1/system.d/nm-ssh-service.conf
%{_prefix}/lib/NetworkManager/VPN/nm-ssh-service.name
%{_libexecdir}/nm-ssh-service
%{_libexecdir}/nm-ssh-auth-dialog
%doc AUTHORS README ChangeLog NEWS
%license COPYING

%files -n NetworkManager-ssh-gnome
%{_libdir}/NetworkManager/lib*.so*
%dir %{_datadir}/gnome-vpn-properties/ssh
%{_datadir}/gnome-vpn-properties/ssh/nm-ssh-dialog.ui
%{_datadir}/appdata/network-manager-ssh.metainfo.xml

%if %with libnm_glib
%{_sysconfdir}/NetworkManager/VPN/nm-ssh-service.name
%endif

%changelog
* Tue Dec 03 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 1.2.12-2
- Fix configure.ac error causing compilation failure

* Thu May 04 2023 misaka00251 <liuxin@iscas.ac.cn> - 1.2.12-1
- Init package
